# 2. Візьміть функції з попереднього ДЗ, покладіть їх у файл lib.py і імпортуйте в основний файл для виконання

def get_date() -> str:
    """
    This function asks for the date (day and month)
    and then returns the received data in a single str.
    If the data is not in the range 1 to 31 or
    the month is not in the range 1 to 12, asks to enter the data again.
    If the entered data is not a number, it asks to enter the data again

    :return: date as single str
    """
    while True:
        day = input('Enter a day: ')
        month = input('Enter a month: ')

        if day.isdigit() and month.isdigit() and 1 <= int(day) <= 31 and 1 <= int(month) <= 12:
            break
        else:
            print('Error! It\'s not a correct day. Try again, please.')

    entered_date = f'{day}.{month}'

    return entered_date


def definition_of_the_season(date: str) -> str:
    """
    This function receives a date as date and month str
    and converts it into a list. Then, based on the second
    value in the list([1] second but by number one),
    it calculates what time of year it is

    :param date: as date and month str
    :return: str of time of year
    """
    split_date = date.split('.')

    if int(split_date[1]) in range(3, 5):
        return 'Spring'

    elif int(split_date[1]) in range(6, 8):
        return 'Summer'

    elif int(split_date[1]) in range(9, 11):
        return 'Autumn'
    else:
        return 'Winter'


if __name__ == '__main__':
    print(definition_of_the_season(get_date()))
