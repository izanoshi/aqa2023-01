# 1. Напишіть декоратор, який визначає час виконання функції.
# Заміряйте час іиконання функцій з попереднього ДЗ
import time


def timer(func):
    """
    This is a decorator for calculating the running time of a function
    """
    def _wrapper(*args, **kwargs):
        start_time = time.time()
        result = func(*args, **kwargs)
        end_time = time.time()
        print('Function execution time: %f' % (end_time - start_time))
        return result

    return _wrapper


@timer
def function_for_2_args(arg1: str, arg2: str) -> int | str | tuple:
    """
    This function, depending on the type of the two arguments that were entered,
    performs one of three options with them.
    if both int - multiplies two numbers
    if both str - connotation
    if one of them is int and the other is str, it creates a tuple of them
    :param arg1: str
    :param arg2: str
    :return: depending on the entered data, we can get one of three results - int or str or tuple
    """
    if arg1.isdigit() and arg2.isdigit():
        return int(arg1) * int(arg2)

    elif arg1.isalpha() and arg2.isalpha():
        return f'{arg1} and {arg2}'

    else:
        result = [arg1, arg2]
        return tuple(result)


print(function_for_2_args('147465', '254'))
print(function_for_2_args('dxs', 'dfdxzw'))
print(function_for_2_args('1edff', '233'))


# 2. Візьміть функції з попереднього ДЗ, покладіть їх у файл lib.py і імпортуйте в основний файл для виконання
import lib


print(lib.definition_of_the_season(lib.get_date()))

# or
from lib import get_date
from lib import definition_of_the_season


print(definition_of_the_season(get_date()))
