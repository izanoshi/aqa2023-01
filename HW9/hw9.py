# 1. Створіть клас "Транспортний засіб" та підкласи "Автомобіль", "Літак", "Корабель",
# наслідувані від "Транспортний засіб". Наповніть класи атрибутами на свій розсуд.
# Створіть обʼєкти класів "Автомобіль", "Літак", "Корабель".

class Vehicle:
    year = 2017

    def say_hello(self, name='User'):
        message = f'Hi, {name}'
        return message


class Car(Vehicle):
    Chevrolet = 'Chevrolet Camaro'

    def what_was_created_Car(self):
        message = f'What did the Chevrolet create in {self.year}? - {self.Chevrolet}'
        return message


class Airplane(Vehicle):
    Boeing = '737 Max 8'

    def what_was_created_Airplane(self):
        message = f'What did the Boeing create in {self.year}? - {self.Boeing}'
        return message


class Ship(Vehicle):
    Bogazici = 'Bugsier 12'

    def what_was_created_Ship(self):
        message = f'What did the Bogazici create in {self.year}? - {self.Bogazici}'
        return message


car = Car()
airplane = Airplane()
ship = Ship()

print(car.say_hello('Ann'))
print(car.what_was_created_Car())
print(airplane.say_hello())
print(airplane.what_was_created_Airplane())
print(ship.what_was_created_Ship())
