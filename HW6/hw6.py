# 1. Напишіть функцію, яка приймає два аргументи.
# a. Якщо обидва аргумени відносяться до числових типів функція пермножує
# ці аргументи і повертає результат
# b. Якшо обидва аргументи відносяться до типу стрінг функція обʼєднує
# їх в один і повертає
# c. В будь-якому іншому випадку - функція повертає кортеж з двох агрументів


def function_for_2_args(arg1, arg2):

    if arg1.isdigit() and arg2.isdigit():
        return int(arg1) * int(arg2)

    elif arg1.isalpha() and arg2.isalpha():
        return f'{arg1} and {arg2}'

    else:
        result = [arg1, arg2]
        return tuple(result)


print(function_for_2_args('145', '254'))
print(function_for_2_args('dxs', 'dfdxzw'))
print(function_for_2_args('1edff', '233'))


# Візьміть попереднє дз "Касир в кінотеатрі" і перепишіть
# за допомогою функцій. Памʼятайте про SRP!

def get_age():

    while True:
        age = input('Hi. How old are you? Please, enter your age: ')

        if age.isdigit():
            break
        else:
            print('Error! It\'s not a number. Try again, please.')

    age_number = int(age)

    return age_number

def result_of_age_verification(age_number):

    if '7' in str(age_number):
        print('You will be lucky today!')
    elif 0 < age_number < 7:
        print('Where are your parents?')
    elif 8 < age_number < 16:
        print('This is a movie for adults!')
    elif age_number > 65:
        print('Show your pension certificate!')
    else:
        print('Sorry, but there are no more tickets!')


result_of_age_verification(get_age())