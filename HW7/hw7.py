# 1. Напишіть функцію, яка визначає сезон за датою.
# Функція отримує стрінг у форматі "[день].[місяць]"
# (наприклад "12.01", "30.08", "1.11" і тд) і повинна повернути стрінг
# з відповідним сезоном, до якого відноситься ця дата
# ("літо", "осінь", "зима", "весна")
#

def get_date() -> str:
    """
    This function asks for the date (day and month)
    and then returns the received data in a single str.
    If the data is not in the range 1 to 31 or
    the month is not in the range 1 to 12, asks to enter the data again.
    If the entered data is not a number, it asks to enter the data again

    :return: date as single str
    """
    while True:
        day = input('Enter a day: ')
        month = input('Enter a month: ')

        if day.isdigit() and month.isdigit() and 1 <= int(day) <= 31 and 1 <= int(month) <= 12:
            break
        else:
            print('Error! It\'s not a correct day. Try again, please.')

    entered_date = f'{day}.{month}'

    return entered_date


def definition_of_the_season(date: str) -> str:
    """
    This function receives a date as date and month str
    and converts it into a list. Then, based on the second
    value in the list([1] second but by number one),
    it calculates what time of year it is

    :param date: as date and month str
    :return: str of time of year
    """
    split_date = date.split('.')

    if int(split_date[1]) in range(3, 5):
        return 'Spring'

    elif int(split_date[1]) in range(6, 8):
        return 'Summer'

    elif int(split_date[1]) in range(9, 11):
        return 'Autumn'
    else:
        return 'Winter'


print(definition_of_the_season(get_date()))


# Напишіть функцію "Тупий калькулятор", яка приймає два числових аргументи і строковий, який відповідає за операцію між
# ними (+ - / *). Функція повинна повертати значення відповідної операції (додавання, віднімання, ділення, множення),
# інші операції не допускаються. Якщо функція оримала невірний тип данних для операції (не числа) або неприпустимий
# (невідомий) тип операції вона повинна повернути None і вивести повідомлення "Невірний тип даних" або "Операція не
# підтримується" відповідно.


def get_and_check_number_or_not() -> float:
    """
    This function asks for a number and checks whether this number or not,
     and if not, then requires you to enter a number

    :return: converted the input str to a float
    """
    while True:
        num = input('Enter a number: ')

        if not num.isdigit():
            print('Error! It\'s not a number. Try again, please.')
        else:
            return float(num)
            break


def add(x: float, y: float) -> float:
    """
    This function adds two numbers

    :param x: first number
    :param y: second number
    :return: result of operation
    """
    return x + y


def subtract(x: float, y: float) -> float:
    """
    This function subtracts two numbers

    :param x: first number
    :param y: second number
    :return: result of operation
    """
    return x - y


def divide(x: float, y: float) -> float:
    """
    This function divides two numbers

    :param x: first number
    :param y: second number
    :return: result of operation
    """
    return x / y


def multiply(x: float, y: float) -> float:
    """
    This function multiplies two numbers

    :param x: first number
    :param y: second number
    :return: result of operation
    """
    return x * y


def stupid_calculator() -> float | None:
    """
    This "Stupid Calculator" accepts two numeric arguments and a string that is
    responsible for the operation between them (+ - / *). The function
    must return the value of the corresponding operation
    (addition, subtraction, division, multiplication), other operations are not
    allowed. If the function received an invalid data type for the operation
    (not a number) or an invalid (unknown) type of operation, it should return
    None and display the message "Operation not supported"

    :return: return the value of the corresponding operation as float or None
    """
    while True:
        chosen_operation = input('Choose what you want to do with numbers (+ or - or / or *): ')

        if chosen_operation in ('+', '-', '/', '*'):

            num1 = get_and_check_number_or_not()
            num2 = get_and_check_number_or_not()

            if chosen_operation == '+':
                result = add(num1, num2)
                print(f'Result = {result}')
                return result

            elif chosen_operation == '-':
                result = subtract(num1, num2)
                print(f'Result = {result}')
                return result

            elif chosen_operation == '/':
                if num2 != 0:
                    result = divide(num1, num2)
                    print(f'Result = {result}')
                    return result
                else:
                    print('Division by zero is not possible')

            elif chosen_operation == '*':
                result = multiply(num1, num2)
                print(f'Result = {result}')
                return result

        else:
            print('Operation not supported')
            return None


stupid_calculator()
