from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium import webdriver


class TestUntitled():
    def setup_method(self):
        self.driver = webdriver.Chrome()
        self.vars = {}

    def teardown_method(self):
        self.driver.quit()

    def test_untitled(self):
        self.driver.get("https://uk.wikipedia.org/wiki/")
        self.driver.set_window_size(1185, 1022)
        self.driver.find_element(By.ID, "searchInput").click()
        self.driver.find_element(By.ID, "searchInput").send_keys("Баку")
        self.driver.find_element(By.ID, "searchInput").send_keys(Keys.ENTER)
        self.driver.find_element(By.LINK_TEXT, "Каспійському морі").click()
        self.driver.find_element(By.LINK_TEXT, "Азербайджану").click()
        self.driver.find_element(By.LINK_TEXT, "Південно-східної Європи").click()
        self.driver.find_element(By.LINK_TEXT, "Європи").click()
