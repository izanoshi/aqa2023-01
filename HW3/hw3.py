# 1. Напишіть цикл, який буде вимагати від користувача ввести слово,
#в якому є буква "о" (враховуються як великі так і маленькі).
# Цикл не повинен завершитися, якщо користувач ввів слово без букви "о".

while True:
    word = input('Hi! Please, enter a word: ')

    if 'o' in word or 'O' in word:
        print('Thank you! Have a nice day')
        break
    else:
        print('No! Need a word with o or O. Try again, please.')


# 2. Є list з даними lst1 = ['1', '2', 3, True, 'False', 5, '6', 7, 8, 'Python', 9, 0, 'Lorem Ipsum'].
#Напишіть код, який свормує новий list (наприклад lst2), який містить лише змінні типу стрінг,
# які присутні в lst1. Зауважте, що lst1 не є статичним і може формуватися динамічно
# від запуску до запуску.

lst1 = ['1', '2', 3, True, 'False', 5, '6', 7, 8, 'Python', 9, 0, 'Lorem Ipsum']

lst2 = [i for i in lst1 if isinstance(i, str)]
print(lst2)


# 3. Є стрінг з певним текстом (можна скористатися input або константою).
# Напишіть код, який визначить кількість слів в цьому тексті,
# які закінчуються на "о" (враховуються як великі так і маленькі).

print(len(list(filter(lambda words_with_o: words_with_o.endswith("o"), input('Enter any words: ').lower().split()))))