# Сформуйте стрінг, в якому міститься інформація про певне слово.
# "Word [тут слово] has [тут довжина слова, отримайте з самого сдлва] letters",
# наприклад "Word 'Python' has 6 letters".
# Для отримання слова для аналізу скористайтеся константою або функцією input().

informational_text = 'Word {} has {} letters'

while True:
    word = input('Enter a word: ')

    if word.isalpha():
        word_length = len(word)
        result = informational_text.format(word, word_length)
        break
    else:
        print('Error! It\'s not a word. Try again, please.')

print(result)


# Напишіть программу "Касир в кінотеватрі", яка попросіть користувача ввести свсвій вік
# (можно використати константу або функцію input(), на екран має бути виведено лише одне повідомлення,
# також подумайте над варіантами, коли введені невірні дані).
# якщо користувачу менше 7 - вивести повідомлення"Де твої батьки?"
# якщо користувачу менше 16 - вивести повідомлення "Це фільм для дорослих!"
# якщо користувачу більше 65 - вивести повідомлення"Покажіть пенсійне посвідчення!"
# якщо вік користувача містить цифру 7 - вивести повідомлення "Вам сьогодні пощастить!"
# у будь-якому іншому випадку - вивести повідомлення "А білетів вже немає!"

while True:
    age = input('Hi. How old are you? Please, enter your age: ')

    if age.isdigit():
        break
    else:
        print('Error! It\'s not a number. Try again, please.')

age_number = int(age)

if '7' in age:
    print('You will be lucky today!')
elif 0 < age_number < 7:
    print('Where are your parents?')
elif 8 < age_number < 16:
    print('This is a movie for adults!')
elif age_number > 65:
    print('Show your pension certificate!"')
else:
    print('Sorry, but there are no more tickets!')

