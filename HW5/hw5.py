"""
Завдання 1
Макс 40 балів
написати програму, яка просить в користувача ввести через пробіл міста, в яких він був за минулі 10 років
потім окремо запросити у користувача міста, куди він хоче поїхати внаступні 10 років
вивести на екран повідомлення з текстом про те, що користувачу, мабуть, дуже сподобалося в містах, які він повторив в
двох циклах вводу, а саме... (сформувати строку, використовуючи join)
якщо повтору не було - вивести повідомлення, що користувач відкритий до чогось нового

врахувати випадки, що користувач нічого не вводить не потрібно, в даному випадку вам явно зазначено,
що ці перевірки не потрібні.
не потрібно перевіряти введення цифр
ми виходим із того, що користувач введе щось на зразок "Київ Тернопіль париЖ акапулько-80"

В той же час врахуйте, що користувач може вводити дані в різних регістрах

використати сети!!!
"""

cities_was = input('Hi. Please, write the cities you have been in the last 10 years (Add spaces between words): ').split()
cities_will = input('Please, write the cities you want to visit in the next 10 years (Add spaces between words): ').split()

set_cities_was = set(map(str.lower, cities_was))
set_cities_will = set(map(str.lower, cities_will))

common_in_both_cities_sets = set_cities_was & set_cities_will

if common_in_both_cities_sets:
    print('Wow, you are probably really liked these cities: '+', '.join(common_in_both_cities_sets).title())
else:
    print('Cool, you are open to new impressions')


"""
Завдання 2
макс 60 балів
зауважте, що значення, що зберігається в кожному елементі - теж словник, і доступ до вкладеного списку 
здійснюється за механізмом 
student[outer_dict_key][inner_dict_key]

Є дані студентів (комбінація імені та прізвища унікальна), що зберігаються за допомогою словника
1 - програмно добавити одного студента, з заповненням усіх полів (вік - від 18 до 40, цілочисельне значення, 
    бал від 0 до 100 (інт чи флоат)
2 - створити і вивести на екран список студентів (імя та прізвище та середній бал), у яких середній бал більше 90
    сам формат наповнення цього списку up to you
3 - визначити середній бал по групі
4 - при відсутності номеру телефону у студента записати номер батьків (номер на ваш вибір)

не забувайте виводити інформаційні повідомлення щодо інформації, яку ви виводите
"""
student = {
    'Іван Петров': {
        'Пошта': 'Ivan@gmail.com',
        'Вік': 14,
        'Номер телефону': '+380987771221',
        'Середній бал': 95.8
    },
    'Женя Курич': {
        'Пошта': 'Geka@gmail.com',
        'Вік': 16,
        'Номер телефону': None,
        'Середній бал': 64.5
    },
    'Маша Кера': {
        'Пошта': 'Masha@gmail.com',
        'Вік': 18,
        'Номер телефону': '+380986671221',
        'Середній бал': 80
    },
}

# 1

student['Вася Пупкін'] = {'Пошта': 'ivan@gmail.com', 'Вік': 21, 'Номер телефону': '+380637856984', 'Середній бал': 97}
# or
student.update({'Вася Пупкін': {'Пошта': 'ivan@gmail.com', 'Вік': 21, 'Номер телефону': '+380637856984', 'Середній бал': 97}})

# 2
best_students = {}

for students_name, GPA in student.items():
    if GPA['Середній бал'] > 90:
        best_students[students_name] = GPA['Середній бал']
    else:
        continue

print(f'Students with a GPA above 90 - {best_students}')

# 3
common_GPA = 0

for students_name, GPA in student.items():
    common_GPA += GPA['Середній бал']/len(student)

print(f'The GPA of the group is - {common_GPA}')

# 4

numbers_of_students_parents = {
    'Іван Петров': '+380975674837',
    'Женя Курич': '+380965678495',
    'Маша Кера': '+38065478793'
}

for students_name, phone_number in student.items():
    if phone_number['Номер телефону'] is None:
        print(f'A student\'s name with a missing number - {students_name}')
        phone_number['Номер телефону'] = numbers_of_students_parents[students_name]
        print(f'A parents number has been added to the {students_name} portfolio. Check data - {student[students_name]}')
    else:
        continue
