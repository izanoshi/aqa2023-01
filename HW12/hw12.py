# 1. Доопрацюйте класс Pоint з наняття наступним чином:
# додайте перевірку координат x та y на числа за допомогою property


class Point:
    x = None
    y = None

    def __init__(self, x_coord, y_coord):
        self.x = x_coord
        self.y = y_coord

    def point_getter(self):
        if not isinstance(self.x, (int, float)) or not isinstance(self.y, (int, float)):
            raise TypeError

        return f'Point with coords - {self.x}:{self.y}'

    point_property = property(point_getter)


point_1 = Point(0, 3)
point_2 = Point('10', 10)

print(point_1.point_property)
print(point_2.point_property)

# 2. Доопрацюйте класс Line з наняття наступним чином:
# додайте можливість порівнювати лінії по довжині (==, !=, >=, <=, >, <)
# за допомогою відповідних методів


point1 = Point(0, 0)
point2 = Point(10, 10)
point3 = Point(3, 0)
point4 = Point(15, 16)


class Line:
    begin = None
    end = None

    def __init__(self, begin_point, end_point):
        self.begin = begin_point
        self.end = end_point

    def __str__(self):
        return f'Line with coords [{self.begin.x}-{self.begin.y}:{self.end.x}-{self.end.y}]'

    def __eq__(self, other):
        if not isinstance(other, type(self)):
            raise TypeError

        is_begin_match = self.begin.x == other.begin.x and self.begin.y == other.begin.y
        is_end_match = self.end.x == other.end.x and self.end.y == other.end.y

        return is_begin_match and is_end_match

    def __ne__(self, other):
        if not isinstance(other, type(self)):
            raise TypeError

        begin = self.begin.x != other.begin.x or self.begin.y != other.begin.y
        end = self.end.x != other.end.x or self.end.y != other.end.y

        return begin and end

    def __lt__(self, other):
        if not isinstance(other, type(self)):
            raise TypeError

        begin = self.begin.x < other.begin.x or self.begin.y < other.begin.y
        end = self.end.x < other.end.x or self.end.y < other.end.y

        return begin and end

    def __gt__(self, other):
        if not isinstance(other, type(self)):
            raise TypeError

        begin = self.begin.x > other.begin.x or self.begin.y > other.begin.y
        end = self.end.x > other.end.x or self.end.y > other.end.y

        return begin and end

    def __le__(self, other):
        if not isinstance(other, type(self)):
            raise TypeError

        begin = self.begin.x <= other.begin.x and self.begin.y <= other.begin.y
        end = self.end.x <= other.end.x and self.end.y <= other.end.y

        return begin and end

    def __ge__(self, other):
        if not isinstance(other, type(self)):
            raise TypeError

        begin = self.begin.x >= other.begin.x and self.begin.y >= other.begin.y
        end = self.end.x >= other.end.x and self.end.y >= other.end.y

        return begin and end


line1 = Line(point1, point2)
line2 = Line(point3, point4)

print(line1)
print(line2)

print(
    line1 < line2,
    line1 > line2,
    line1 <= line2,
    line1 >= line2,
    line1 == line2,
    line1 != line2
)
