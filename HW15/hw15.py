# Підключіться до API НБУ ( документація тут https://bank.gov.ua/ua/open-data/api-dev ),
# отримайте курс валют і запишіть його в текстовий файл такому форматі (список):

# "[дата створення запиту]"
# 1. [назва валюти 1] to UAH: [значення курсу до валюти 1]
# 2. [назва валюти 2] to UAH: [значення курсу до валюти 2]
# 3. [назва валюти 3] to UAH: [значення курсу до валюти 3]
# ...

# n. [назва валюти n] to UAH: [значення курсу до валюти n]

# P.S.не забувайте про DRY, KISS, SRP та перевірки
import requests


request = requests.get('https://bank.gov.ua/NBUStatService/v1/statdirectory/exchangenew?json')
list_of_dates = request.json()


with open('data.txt', 'w') as f:
    for i in range(len(list_of_dates)):
        print(f'{i+1}.', list_of_dates[i]["txt"], 'to UAH:', list_of_dates[i]["rate"], file=f)
