# 1. Задача: Створіть дві змінні first=10, second=30.
# Виведіть на екран результат математичної взаємодії (+, -, *, / и тд.) для цих чисел.

first = 10
second = 30

print(first + second)
print(first - second)
print(second - first)
print(first * second)
print(first / second)
print(second / first)
print(first // second)
print(second // first)
print(first ** second)
print(second ** first)
print(first % second)
print(second % first)

# or

result = first + second
print(result)

result = first - second
print(result)

result = second - first
print(result)

result = first * second
print(result)

result = first / second
print(result)

result = second / first
print(result)

result = first // second
print(result)

result = second // first
print(result)

result = first ** second
print(result)

result = second ** first
print(result)

result = first % second
print(result)

result = second % first
print(result)


# 2. Створіть змінну и по черзі запишіть в неї результат порівняння
# (<, > , ==, !=) чисел з завдання 1.
# Виведіть на екран результат кожного порівняння.

comparison = first < second
print(comparison)

comparison = first > second
print(comparison)

comparison = first == second
print(comparison)

comparison = first != second
print(comparison)


# 3. Створіть змінну - резкльтат конкатенації строк "Hello " та "world!".

str1 = 'Hello '
str2 = 'world!'

str3 = str1 + str2
print(str3)